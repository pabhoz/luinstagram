<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_controller
 *
 * @author pabhoz
 */
class User_controller extends BController{
    //put your code here
    public function index() {
        
    }
    
    public function follow(){
        $id1 = $_GET["me"];
        $id2 = $_GET["other"];
        
        $usr1 = User::getById($id1);
        $usr2 = User::getById($id2);
        
        $usr1->has_many("FollowedUsers", $usr2 ,array(
            "when"=>null
            )
        );
        
        //print_r($usr1);
        $usr1->update();
    }
    
    public function whoFollowWho(){
        $usrs = Users_bl::whoFollowWho();
        print_r($usrs);
    }

}
