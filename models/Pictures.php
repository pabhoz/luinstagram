<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pictures
 *
 * @author pabhoz
 */
class Pictures extends BModel {
    
   private $id;
   private $uri;
   private $likes;
   private $desc;
   private $userId;
   private $date;
   
   private $has_one = array(
      'Owner'=>array(
          'class'=>'User',
          'join_as'=>'userId',
          'join_with'=>'id'
          )
      );
  
  private $known_as = array(
        
            'Owner' => array(
                'class' => 'Comment',
                'join_as' => 'id',
                'join_with' => 'pictureId'
            )
        
        );
   
   public function __construct($id,string $uri,int $likes,string $desc, $userId,
           string $date) {
       parent::__construct();
       $this->id = $id;
       $this->uri = $uri;
       $this->likes = $likes;
       $this->desc = $desc;
       $this->userId = $userId;
       $this->date = $date;
   }
   
   public function getId() {
       return $this->id;
   }

   public function getUri() {
       return $this->uri;
   }

   public function getLikes() {
       return $this->likes;
   }

   public function getDesc() {
       return $this->desc;
   }

   public function getUserId() {
       return $this->userId;
   }

   public function getDate() {
       return $this->date;
   }

   public function setId($id) {
       $this->id = $id;
   }

   public function setUri($uri) {
       $this->uri = $uri;
   }

   public function setLikes($likes) {
       $this->likes = $likes;
   }

   public function setDesc($desc) {
       $this->desc = $desc;
   }

   public function setUserId($userId) {
       $this->userId = $userId;
   }

   public function setDate($date) {
       $this->date = $date;
   }

    public function getMyVars() {
        return get_object_vars($this);
    }

}
