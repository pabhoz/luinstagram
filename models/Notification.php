<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Notification
 *
 * @author pabhoz
 */
class Notification extends BModel{
    private $id;
    private $owner;
    private $event;
    private $url;
    
    private $has_one = array(
      'Owner'=>array(
          'class'=>'User',
          'join_as'=>'owner',
          'join_with'=>'id'
          )
      );
    
    public function __construct($id, $owner, $event, $url) {
        parent::__construct();
        $this->id = $id;
        $this->owner = $owner;
        $this->event = $event;
        $this->url = $url;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getOwner() {
        return $this->owner;
    }

    public function getEvent() {
        return $this->event;
    }

    public function getUrl() {
        return $this->url;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setOwner($owner) {
        $this->owner = $owner;
    }

    public function setEvent($event) {
        $this->event = $event;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function getMyVars() {
       return get_object_vars($this); 
    }
    public function getHas_one() {
        return $this->has_one;
    }

    public function setHas_one($has_one) {
        $this->has_one = $has_one;
    }


}
