<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Comment
 *
 * @author pabhoz
 */
class Comment extends BModel{
    private $id;
    private $userId;
    private $date;
    private $comment;
    private $pictureId;
    
    private $has_one = array(
      'User'=>array(
          'class'=>'User',
          'join_as'=>'userId',
          'join_with'=>'id'
          ),
        'Picture'=>array(
          'class'=>'Picture',
          'join_as'=>'pictureId',
          'join_with'=>'id'
          )
      );
    
    public function __construct($id, $userId, $date, $comment, $pictureId) {
        parent::__construct();
        $this->id = $id;
        $this->userId = $userId;
        $this->date = $date;
        $this->comment = $comment;
        $this->pictureId = $pictureId;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function getDate() {
        return $this->date;
    }

    public function getComment() {
        return $this->comment;
    }

    public function getPictureId() {
        return $this->pictureId;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function setComment($comment) {
        $this->comment = $comment;
    }

    public function setPictureId($pictureId) {
        $this->pictureId = $pictureId;
    }

    public function getMyVars() {
       return get_object_vars($this); 
    }
    
    public function getHas_one() {
        return $this->has_one;
    }

    public function setHas_one($has_one) {
        $this->has_one = $has_one;
    }



}
