<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author pabhoz
 */
class User extends BModel {

    private $id, $username, $password;
    private $known_as = array(
        'NotificationOwner' => array(
            'class' => 'Notification',
            'join_as' => 'id',
            'join_with' => 'owner'
        ),
        'PictureOwner' => array(
            'class' => 'Picture',
            'join_as' => 'id',
            'join_with' => 'userId'
        ),
        'CommentOwner' => array(
            'class' => 'Comment',
            'join_as' => 'id',
            'join_with' => 'userId'
        )
    );
    private $has_many = array(
        'FollowedUsers' => array(
            'class' => 'User',
            'my_key' => 'id',
            'other_key' => 'id',
            'join_as' => 'me',
            'join_with' => 'other',
            'join_table' => 'Following',
            'data' => array(
                'when' => 'TIMESTAMP' // 'aFloat' => 0.0, 'aString' => '' 
            )
        )
    );

    public function __construct($id, $username, $password) {

        parent::__construct();

        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
    }

    public function getId() {
        return $this->id;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getMyVars() {
        return get_object_vars($this);
    }
    
    public function getKnown_as() {
        return $this->known_as;
    }

    public function getHas_many() {
        return $this->has_many;
    }

    public function setKnown_as($known_as) {
        $this->known_as = $known_as;
    }

    public function setHas_many($has_many) {
        $this->has_many = $has_many;
    }



}
