<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModelFactory
 *
 * @author pabhoz
 */
abstract class BController extends Controller implements IController{ 
    
    public function __construct() {
        parent::__construct();
        Session::init();
    }
    abstract function index();
    
}
